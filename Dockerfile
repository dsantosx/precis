FROM golang:1.11.5-alpine as builder
RUN apk add --no-cache ca-certificates git
WORKDIR /src
COPY ./ ./
RUN CGO_ENABLED=0 go build -mod=vendor -o /precis cmd/precis/*

FROM scratch AS final
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /src/certs/cert.pem /cert.pem
COPY --from=builder /src/certs/key.pem /key.pem
COPY --from=builder /precis /precis
USER nobody
EXPOSE 3000
ENTRYPOINT ["/precis"]