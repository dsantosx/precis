# Precis

Precis is a tiny service that persists request data in a redis database and generates a token to be used to expand the request for further use.
This can useful when there is a need for sending SMS's containing short urls that when clicked would expand the data to perform another external request or autofil a form with data, for example, PII.

## Requirements  
- docker
- docker-compose
- AWS account (needed for batch processing) - set credentials in `docker-compose.yml`

## Build and Run
    docker-compose up --build
This step will build and run the precis container together with a redis container.

## Example usage

#### Encode precis data

POST https://localhost:3000/encode?expires=2018-10-03
```json
{
    "op": "zeus",
    "single_use": false,
    "data": [
        {
            "type": "foo",
            "qty": 3,
            "sku": 24950138
        },
        {
            "type": "bar",
            "qty": 3,
            "sku": 87654353
        }
    ]
}
```

Response:
```json
{
    "token": "1qV1B2amlne",
    "expires": "2019-02-13T00:00:00Z"
}
```

#### Encode precis batch:

https://localhost:3000/encode/batch
```json
{
	"bucket": "bucket-name",
	"key": "folder/batch-file.csv"
}
```

`batch-file.csv` example:

```csv
id;precis
1234567890ABCD;{"op":"addition","data":["foo", "bar"]}
```
it would upload a file to the same bucket as
```csv
id;precis;token
1234567890ABCD;{"op":"addition","data":["foo", "bar"]};1qV2k4myn0I
```

#### Decode precis token

GET https://localhost:3000/decode/1qV1B2amlne

Response:
```json
{
    "op": "zeus",
    "single_use": false,
    "data": [
        {
            "type": "foo",
            "qty": 3,
            "sku": 24950138
        },
        {
            "type": "bar",
            "qty": 3,
            "sku": 87654353
        }
    ]
}
```

### TODO
- [ ] Add more tests
- [ ] Implement single use: precis expires after being used once
- [ ] Change service functions to accept/return types
- [ ] Make precis request more generic
- [ ] When saving a batch, implement a strategy to return an error if the object does not exist, instead of 201, channels?
- [ ] Add batch file parsing validation
- [ ] Set expire for batch processing