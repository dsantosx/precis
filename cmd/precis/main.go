package main

import (
	"bitbucket.org/duartesantosx/precis/precis"
	"bitbucket.org/duartesantosx/precis/server"
	"bitbucket.org/duartesantosx/precis/storage/redis"
	"github.com/aws/aws-sdk-go/aws/session"
	log "github.com/sirupsen/logrus"
	"gopkg.in/alecthomas/kingpin.v2"
	"net/http"
	"time"
)

var (
	version    = "0.0.1"
	listenAddr = kingpin.Flag("listen-addr", "Address to listen on").Default(":3000").String()
	redisAddr  = kingpin.Flag("redis-addr", "Redis address and port").Default("localhost:6379").String()
	certFile   = kingpin.Flag("cert-file", "SSL Certificate").Default("cert.pem").String()
	keyFile    = kingpin.Flag("key-file", "SSL Private key").Default("key.pem").String()
	debug      = kingpin.Flag("debug", "Enable debug mode").Bool()
)

func main() {
	kingpin.Version(version)
	kingpin.Parse()

	log.SetFormatter(&log.TextFormatter{
		FullTimestamp:   true,
		TimestampFormat: time.RFC3339,
	})
	if *debug {
		log.SetLevel(log.DebugLevel)
	}
	log.WithFields(log.Fields{
		"version":  version,
		"loglevel": log.GetLevel(),
	}).Infof("starting application")

	redis, err := redis.New(*redisAddr)
	if err != nil {
		log.WithError(err).Fatal("failed to connect to redis")
	}

	session := session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
	}))

	svc := precis.NewService(redis, session)
	h := precis.NewHandler(svc)

	mux := http.NewServeMux()
	h.SetupRoutes(mux)

	server := server.New(*listenAddr, mux)

	log.WithField("port", *listenAddr).Infof("starting https server")

	if err := server.ListenAndServeTLS(*certFile, *keyFile); err != nil {
		log.WithError(err).Fatal("failed to start https server")
	}
}
