module bitbucket.org/duartesantosx/precis

require (
	github.com/alecthomas/template v0.0.0-20160405071501-a0175ee3bccc // indirect
	github.com/alecthomas/units v0.0.0-20151022065526-2efee857e7cf // indirect
	github.com/alicebob/gopher-json v0.0.0-20180125190556-5a6b3ba71ee6 // indirect
	github.com/alicebob/miniredis v2.4.6+incompatible
	github.com/aws/aws-sdk-go v1.16.32
	github.com/go-playground/locales v0.12.1 // indirect
	github.com/go-playground/universal-translator v0.16.0 // indirect
	github.com/go-redis/redis v6.15.1+incompatible
	github.com/gomodule/redigo v2.0.0+incompatible // indirect
	github.com/leodido/go-urn v1.1.0 // indirect
	github.com/mattheath/base62 v0.0.0-20150408093626-b80cdc656a7a
	github.com/onsi/ginkgo v1.7.0 // indirect
	github.com/onsi/gomega v1.4.3 // indirect
	github.com/sirupsen/logrus v1.3.0
	github.com/yuin/gopher-lua v0.0.0-20190206043414-8bfc7677f583 // indirect
	golang.org/x/net v0.0.0-20190206173232-65e2d4e15006 // indirect
	gopkg.in/alecthomas/kingpin.v2 v2.2.6
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gopkg.in/go-playground/validator.v9 v9.27.0
)
