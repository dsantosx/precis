package precis

import (
	"encoding/csv"
	"fmt"
	"io"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

// BatchSaver saves a batch in the storage
type BatchSaver interface {
	Save(interface{}, time.Time) (string, error)
}

// Batch represents a csv batch file
type Batch struct {
	key    string
	csv    *csv.Reader
	saver  BatchSaver
	header bool
}

// Read implements the io.Reader interface
func (b *Batch) Read(p []byte) (n int, err error) {
	record, err := b.csv.Read()
	if err != nil {
		if err == io.EOF {
			return 0, io.EOF
		}
		return 0, err
	}
	return copy(p, b.process(record)), nil
}

func (b *Batch) process(cols []string) string {
	if b.header {
		b.header = false
		cols = append(cols, "token")
	} else {
		token, err := b.saver.Save(cols[1], time.Time{})
		if err != nil {
			return ""
		}
		cols = append(cols, token)
	}

	return strings.Join(cols, string(b.csv.Comma)) + "\n"
}

// NewBatch creates a new batch to be processed
func NewBatch(key string, r io.Reader, saver BatchSaver) *Batch {
	newKey := generateFilename(key, time.Now())
	if newKey == "" {
		newKey = "unknown-key.csv"
	}

	batch := &Batch{
		key:    newKey,
		csv:    csv.NewReader(r),
		saver:  saver,
		header: true,
	}

	batch.csv.Comma = ';'
	batch.csv.LazyQuotes = true

	return batch
}

func generateFilename(key string, now time.Time) string {
	_, file := filepath.Split(key)
	return strings.Replace(file, ".csv", fmt.Sprintf("-%s.csv", strconv.FormatInt(now.UTC().UnixNano(), 10)), 1)
}
