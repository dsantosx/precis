package precis

import (
	"reflect"
	"testing"
	"time"
)

func TestGenerateFilename(t *testing.T) {
	now := time.Date(1974, time.May, 19, 1, 2, 3, 4, time.UTC)
	var tests = []struct {
		in  string
		out string
	}{
		{"", ""},
		{"file", "file"},
		{"file.csv", "file-138157323000000004.csv"},
		{"folder/file.csv", "file-138157323000000004.csv"},
		{"folder/folder/file.csv", "file-138157323000000004.csv"},
	}
	for _, tt := range tests {
		s := generateFilename(tt.in, now)
		equals(t, tt.out, s)
	}
}

func equals(tb testing.TB, exp, act interface{}) {
	tb.Helper()
	if !reflect.DeepEqual(exp, act) {
		tb.Errorf("expected: %#v got: %#v", exp, act)
	}
}
