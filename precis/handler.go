package precis

import (
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gopkg.in/go-playground/validator.v9"
	"net/http"
	"time"
)

const dateFormat string = "2006-01-02"

// Handler is the precis handler
type Handler struct {
	service   Service
	validator *validator.Validate
}

// Encode handles the request to encode a precis
func (h Handler) Encode(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		jsonResponse(w, http.StatusBadRequest, nil)
		return
	}

	exp := r.URL.Query().Get("expires")
	if exp == "" {
		jsonResponse(w, http.StatusBadRequest, "The 'expires' parameter is required")
		return
	}
	expTime, err := time.Parse(dateFormat, exp)
	if err != nil {
		jsonResponse(w, http.StatusBadRequest, "The 'expires' parameter format must be yyyy-mm-dd")
		return
	}

	var p Request
	err = json.NewDecoder(r.Body).Decode(&p)
	if err != nil {
		log.WithError(err).Error("invalid request")
		jsonResponse(w, http.StatusBadRequest, nil)
		return
	}
	defer r.Body.Close()

	err = h.validator.Struct(p)
	if err != nil {
		log.WithError(err).Error("invalid request")
		jsonResponse(w, http.StatusBadRequest, nil)
		return
	}

	token, err := h.service.Save(p, expTime)
	if err != nil {
		log.WithError(err).Errorf("failed to save: %v", p)
		jsonResponse(w, http.StatusInternalServerError, nil)
		return
	}

	jsonResponse(w, http.StatusCreated, Response{
		Token:   token,
		Expires: expTime,
	})
}

// EncodeBatch handles the request to encode a csv precis batch file
func (h Handler) EncodeBatch(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		jsonResponse(w, http.StatusBadRequest, nil)
		return
	}

	var data struct {
		Bucket string
		Key    string
	}
	err := json.NewDecoder(r.Body).Decode(&data)
	if err != nil {
		jsonResponse(w, http.StatusBadRequest, nil)
		return
	}
	defer r.Body.Close()

	go func() {
		err := h.service.SaveBatch(data.Bucket, data.Key)
		if err != nil {
			log.WithFields(
				log.Fields{"bucket": data.Bucket, "key": data.Key},
			).WithError(err).Error("failed to save batch")
		}
	}()

	jsonResponse(w, http.StatusCreated, nil)
}

// Decode handles the request to decode a precis token
func (h Handler) Decode(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		jsonResponse(w, http.StatusBadRequest, nil)
		return
	}

	token := r.URL.Path[len("/decode/"):]
	if token == "" {
		jsonResponse(w, http.StatusBadRequest, "The token path parameter is required")
		return
	}

	val, err := h.service.Load(token)
	if err != nil {
		log.WithField("token", token).Debug("token not found")
		jsonResponse(w, http.StatusNotFound, fmt.Sprintf("token not found: %s", token))
		return
	}

	var p Request
	if err = json.Unmarshal([]byte(val), &p); err != nil {
		log.WithField("value", val).WithError(err).Error("failed to decode token value")
		jsonResponse(w, http.StatusInternalServerError, nil)
		return
	}

	jsonResponse(w, http.StatusOK, p)
}

func jsonResponse(w http.ResponseWriter, code int, body interface{}) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	if body == nil && code > 399 && code < 599 {
		body = http.StatusText(code)
	}
	if msg, ok := body.(string); ok {
		body = map[string]string{
			"error": msg,
		}
	}
	if body != nil {
		json.NewEncoder(w).Encode(body)
	}
}

// Logger is the logging handler middleware
func (h Handler) Logger(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		log.Info(r.URL.Path)
		next(w, r)
	}
}

// SetupRoutes configures the mux with precis routes
func (h Handler) SetupRoutes(mux *http.ServeMux) {
	mux.HandleFunc("/encode", h.Logger(h.Encode))
	mux.HandleFunc("/encode/", h.Logger(h.Encode))
	mux.HandleFunc("/encode/batch", h.Logger(h.EncodeBatch))
	mux.HandleFunc("/encode/batch/", h.Logger(h.EncodeBatch))
	mux.HandleFunc("/decode/", h.Logger(h.Decode))
}

// NewHandler creates a new precis handler
func NewHandler(service Service) *Handler {
	return &Handler{
		service:   service,
		validator: validator.New(),
	}
}
