package precis

import (
	"encoding/json"
	"time"
)

// Request represents a precis
type Request struct {
	Op        string `json:"op" validate:"required"`
	SingleUse bool   `json:"single_use"`
	Data      []struct {
		Type string `json:"type"`
		Qty  int32  `json:"qty"`
		Sku  int32  `json:"sku"`
	} `json:"data,omitempty"`
}

// MarshalBinary implements the encoding.BinaryMarshaler interface
func (r Request) MarshalBinary() ([]byte, error) {
	var d []byte
	d, err := json.Marshal(r)
	if err != nil {
		return nil, err
	}
	return d, nil
}

// Response is a precis token with an expiry date
type Response struct {
	Token   string    `json:"token"`
	Expires time.Time `json:"expires"`
}
