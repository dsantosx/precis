package precis

import (
	"bitbucket.org/duartesantosx/precis/s3"
	"bitbucket.org/duartesantosx/precis/storage"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/mattheath/base62"
	log "github.com/sirupsen/logrus"
	"time"
)

// Service is responsible for precis storage operations
type Service struct {
	encoder *base62.Encoding
	db      storage.Storage
	s3      *s3.S3
}

// Load loads a precis for a given token
func (s Service) Load(token string) (string, error) {
	return s.db.Load(token)
}

// Save saves a precis and returns its token
func (s Service) Save(value interface{}, expire time.Time) (string, error) {
	for {
		token := s.encoder.EncodeInt64(time.Now().UTC().UnixNano())
		ok, err := s.db.Exists(token)
		if err != nil {
			return "", err
		}
		if ok {
			continue
		}

		expire = expire.Add(24 * time.Hour) // day inclusive
		err = s.db.Save(token, value, time.Until(expire))
		if err != nil {
			return "", err
		}

		return token, nil
	}
}

// SaveBatch saves batch precis within a csv from s3
// includes the tokens and uploads it back to s3
func (s Service) SaveBatch(bucket, key string) error {
	body, err := s.s3.DownloadObject(bucket, key)
	if err != nil {
		return err
	}
	defer body.Close()

	batch := NewBatch(key, body, s)

	location, err := s.s3.UploadObject(bucket, batch.key, batch)
	if err != nil {
		return err
	}

	log.Infof("File uploaded to s3: %s", location)
	return nil
}

// NewService creates a new precis service instance
func NewService(db storage.Storage, session *session.Session) Service {
	return Service{
		encoder: base62.StdEncoding,
		db:      db,
		s3:      s3.New(session),
	}
}
