package s3

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"io"
)

// S3 holds the download and upload services
type S3 struct {
	s3       *s3.S3
	uploader *s3manager.Uploader
}

// DownloadObject downloads object key from s3 bucket
func (s S3) DownloadObject(bucket, key string) (io.ReadCloser, error) {
	out, err := s.s3.GetObject(&s3.GetObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
	})
	if err != nil {
		return nil, err
	}
	return out.Body, nil
}

// UploadObject uploads object key to s3 bucket
func (s S3) UploadObject(bucket, key string, r io.Reader) (string, error) {
	out, err := s.uploader.Upload(&s3manager.UploadInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
		Body:   r,
	})
	if err != nil {
		return "", err
	}
	return out.Location, nil
}

// New creates a new s3 service instance
func New(session *session.Session) *S3 {
	return &S3{
		s3:       s3.New(session),
		uploader: s3manager.NewUploader(session),
	}
}
