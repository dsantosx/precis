package redis

import (
	"github.com/go-redis/redis"
	"time"
)

// DB stores the redis client
type DB struct {
	client *redis.Client
}

// Load gets the value of a key
func (db DB) Load(key string) (string, error) {
	return db.client.Get(key).Result()
}

// Save sets the value of a key with an expiration
func (db DB) Save(key string, value interface{}, expire time.Duration) error {
	return db.client.Set(key, value, expire).Err()
}

// Exists determines if key exists
func (db DB) Exists(key string) (bool, error) {
	val, err := db.client.Exists(key).Result()
	if err != nil {
		return false, err
	}
	return val == 1, nil
}

// New creates a new redis storage
func New(addr string) (*DB, error) {
	client := redis.NewClient(&redis.Options{
		Addr: addr,
	})

	if _, err := client.Ping().Result(); err != nil {
		return nil, err
	}

	return &DB{client}, nil
}
