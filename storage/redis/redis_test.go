package redis

import (
	"reflect"
	"testing"
	"time"

	"github.com/alicebob/miniredis"
)

func TestRedis(t *testing.T) {
	mr, err := miniredis.Run()
	ok(t, err)
	defer mr.Close()

	db, err := New(mr.Addr())
	ok(t, err)

	err = db.Save("foo", "bar", 0)
	ok(t, err)
	mr.CheckGet(t, "foo", "bar")

	val, err := db.Load("foo")
	ok(t, err)
	equals(t, "bar", val)

	err = db.Save("egg", "ham", 2*time.Second)
	ok(t, err)
	t.Log(mr.TTL("egg"))

	equals(t, true, mr.Exists("egg"))
	mr.FastForward(2 * time.Second)
	equals(t, false, mr.Exists("egg"))
}

func ok(tb testing.TB, err error) {
	tb.Helper()
	if err != nil {
		tb.Errorf("unexpected error: %s", err.Error())
	}
}

func equals(tb testing.TB, exp, act interface{}) {
	tb.Helper()
	if !reflect.DeepEqual(exp, act) {
		tb.Errorf("expected: %#v got: %#v", exp, act)
	}
}
