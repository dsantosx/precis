package storage

import (
	"time"
)

type Storage interface {
	Save(string, interface{}, time.Duration) error
	Load(string) (string, error)
	Exists(string) (bool, error)
}
